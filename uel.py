#!/usr/bin/python3
import re
import sys
import httplib2
from hyphen import Hyphenator

def main(word=None):
    hypEn = Hyphenator('en_US')
    hypDe = Hyphenator('de_DE')
    if not word:
        req = httplib2.HTTPConnectionWithTimeout('www.wordnik.com', 80)
        req.request('GET', '/randoml')
        res = req.getresponse()
        print(res.getheader('Location'))
        word = re.match(r'.*/(.*)/[^/]*', res.getheader('Location')).groups()[0]
        print(word, file=sys.stderr)
    de = hypDe.syllables(word)
    en = hypEn.syllables(word)
    out = de if len(de) > len(en) else en
    if not out:
        out = [word]
    last = out[-1]
    if len(out) == 1:
        uel = word
    elif word != 'ion' and last[-3:] in ['ion', 'ing'] or word != 'ions' and last[-4:] == 'ions':
        uel = word[:-3]
    elif last[-2:] in ['er', 'el', 'es', 'en', 'ul', 'ed', 'or'] and (len(last) == 2 or last[-3] not in 'aeiouäüöy'):
        uel = word[:-2]
    elif last[-1] in 'aeiouäüöys' and last[-2] not in 'aeiouäüöy' and last[-2:] != 'ss':
        uel = word[:-1]
    else:
        uel = word

    print("%suel" % uel)

amount = None
word = None
if len(sys.argv) > 1:
    try:
        amount = int(sys.argv[1])
    except ValueError:
        word = sys.argv[1]

if amount is not None:
    [main() for i in range(amount)]
else:
    main(word)
