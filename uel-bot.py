#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import sys
import html
import logging
import traceback
import json
import requests
import os.path

from hyphen import Hyphenator

from bs4 import BeautifulSoup
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler


if len(sys.argv) != 3:
    print("Usage: uel-bot.py <api-token> <developer-chat-id>")
    sys.exit(1)


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


DEVELOPER_CHAT_ID = sys.argv[2]  # 17319208


messages = {}


def create_tables():
    """
    conn.execute('''CREATE TABLE IF NOT EXISTS entries (
        msg_id NUMBER PRIMARY KEY,
        content TEXT
    )''')
    conn.execute('''CREATE TABLE IF NOT EXISTS reactions (
        msg_id NUMBER,
        user_id NUMBER,
        reaction TEXT,
        FOREIGN KEY (msg_id) REFERENCES entries(msg_id),
        PRIMARY KEY (msg_id, user_id)
    )''')
    conn.commit()
    """


def save_message(msg_id, text):
    """
    global conn
    conn.execute("INSERT INTO entries (msg_id, content) VALUES (?, ?)", (msg_id, text))
    conn.commit()
    """

    global messages
    messages[msg_id] = {"msg_id": msg_id, "content": text, "reactions": {}, "full": True}


def set_reaction(msg_id, user, reaction):
    """
    global conn
    conn.execute("INSERT INTO reactions (msg_id, user_id, reaction) VALUES (?, ?, ?) ON CONFLICT DO UPDATE SET reaction = ?",
                 (msg_id, user, like, like))
    conn.commit()
    """

    global messages
    if msg_id not in messages:
        save_message(msg_id, "")

    messages[msg_id]["reactions"][user] = reaction


def get_reactions(message):
    """
    global conn
    c = conn.cursor()
    c.execute("SELECT SUM(likes) as likes FROM likes WHERE msg_id = ? GROUP BY msg_id", (message,))
    return c.fetchone()['likes']
    """

    global messages
    reactions = {}
    for reaction in messages[message]["reactions"].values():
        if reaction not in reactions:
            reactions[reaction] = 0
        reactions[reaction] += 1
    return reactions


def mark_next(message):
    global messages
    messages[message]["full"] = False


def get_full(message):
    global messages
    return messages[message]["full"]


"""
conn = sqlite3.connect("uel.db")
create_tables(conn)
"""


hyp_en = Hyphenator('en_US')
hyp_de = Hyphenator('de_DE')


def get_uel(word=None, desc=""):
    if word is not None:
        return word+"uel"

    try:
        r = requests.get("https://www.wordnik.com/randoml")
        r.raise_for_status()
        b = BeautifulSoup(r.content, "html.parser")
        word = b.find("h1", {"id": "headword"}).text.strip()
        desc = "\n" + b.find("div", {"id": "define"}).find("ul").text.strip().replace("\n", " ")
    except Exception as e:
        print(e)
        pass

    if word is None or len(word) == 0:
        word = "man"

    de = hyp_de.syllables(word)
    en = hyp_en.syllables(word)
    out = de if len(de) > len(en) else en
    if not out:
        out = [word]
    last = out[-1]
    if len(out) == 1:
        uel = word
    elif word != 'ion' and last[-3:] in ['ion', 'ing'] or word != 'ions' and last[-4:] == 'ions':
        uel = word[:-3]
    elif last[-2:] in ['er', 'el', 'es', 'en', 'ul', 'ed', 'or'] and (len(last) == 2 or last[-3] not in 'aeiouäüöy'):
        uel = word[:-2]
    elif last[-1] in 'aeiouäüöys' and last[-2] not in 'aeiouäüöy' and last[-2:] != 'ss':
        uel = word[:-1]
    else:
        uel = word

    return f"{uel}uel{desc}"


emojis = [":joy:", ":neutral_face:", ":unamused:"]
mappings = {":joy:":  u'\U0001F602', ":neutral_face:": u'\U0001F610', ":unamused:": u'\U0001F612'}


def get_keyboard(reactions, full=False):
    return [[InlineKeyboardButton((str(reactions[e])+' ' if e in reactions else '') + mappings[e], callback_data=e)
                 for e in emojis]]


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    return send_uel(update, context)


def help_command(update, context):
    """Send a message when the command /help is issued."""
    message = update.message.reply_text(
        "Have some helpuel\n" +
        "you can send suggestions via /uel 'suggestion' which results in 'suggestionuel'\n" +
        "https://gitlab.com/Neverbolt/botuel",
        disable_web_page_preview=True,
    )


def suggestion(update, context):
    """Display a suggestion on /uel"""
    return send_uel(update, context)


def send_uel(update, context):
    """Echo the user message."""
    query = update.callback_query
    if query:
        markup = InlineKeyboardMarkup(get_keyboard(get_reactions(query.message.message_id), False))
        query.answer()
        query.edit_message_text(
            text=query.message.text,
            reply_markup=markup,
        )
        mark_next(query.message.message_id)

    sender = None
    if update.message:
        word = update.message.text
        prefix = None
        if word is not None and word.startswith("/uel "):
            prefix = word[len("/uel "):]
            sender = update.message.reply_text
        elif word == "/start":
            prefix = word
    else:
        prefix = None

    if sender is None:
        def tmp(text, reply_markup):
            return context.bot.send_message(
                update.effective_chat.id,
                text=text,
                reply_markup=reply_markup,
            )
        sender = tmp

    markup = InlineKeyboardMarkup(get_keyboard({}, True))
    message = sender(
        text=get_uel(prefix),
        reply_markup=markup,
    )

    save_message(message.message_id, message.text)


def change_reaction(update, context):
    query = update.callback_query
    query.answer()

    set_reaction(query.message.message_id, query.from_user.id, query.data)
    reactions = get_reactions(query.message.message_id)
    full = get_full(query.message.message_id)

    markup = InlineKeyboardMarkup(get_keyboard(reactions, full))
    query.edit_message_text(
        text=query.message.text,
        reply_markup=markup,
    )


def error_handler(update, context):
    if "Message is not modified" in context.error.message:
        return

    """Log the error and send a telegram message to notify the developer."""
    # Log the error before we do anything else, so we can see it even if something breaks.
    logger.error(msg="Exception while handling an update:", exc_info=context.error)

    # traceback.format_exception returns the usual python message about an exception, but as a
    # list of strings rather than a single string, so we have to join them together.
    tb_list = traceback.format_exception(None, context.error, context.error.__traceback__)
    tb = ''.join(tb_list)

    # Build the message with some markup and additional information about what happened.
    # You might need to add some logic to deal with messages longer than the 4096 character limit.
    message = (
        'An exception was raised while handling an update\n'
        '<pre>update = {}</pre>\n\n'
        '<pre>context.chat_data = {}</pre>\n\n'
        '<pre>context.user_data = {}</pre>\n\n'
        '<pre>{}</pre>'
    ).format(
        html.escape(json.dumps(update.to_dict(), indent=2, ensure_ascii=False)),
        html.escape(str(context.chat_data)),
        html.escape(str(context.user_data)),
        html.escape(tb)
    )

    # Finally, send the message
    context.bot.send_message(chat_id=DEVELOPER_CHAT_ID, text=message, parse_mode=ParseMode.HTML)

def main():
    global messages
    if os.path.isfile("data.json"):
        with open("data.json", "r") as data_in:
            messages = json.loads(data_in.read())

    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(sys.argv[1], use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CallbackQueryHandler(send_uel, pattern="^next$"))
    dp.add_handler(CallbackQueryHandler(change_reaction))
    dp.add_handler(CommandHandler("help", help_command))
    dp.add_handler(CommandHandler("uel", suggestion))
    dp.add_error_handler(error_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

    with open("data.json", "w") as data_out:
        data_out.write(json.dumps(messages))


if __name__ == '__main__':
    main()

